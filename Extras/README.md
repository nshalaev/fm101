# Дополнительные материалы

В этом каталоге вы найдёте дополнительные материалы к курсу. Файлы представлены в трёх
форматах, но основным считается PDF. Файлы Rmd — для тех, кто интересуется, "как это было
сделано".

# Подсказка по оформлению текста в файлах Rmd

Rmd-help.html
Rmd-help.pdf
Rmd-help.Rmd

# Логарифмы в R

Logarithms.html
Logarithms.pdf
Logarithms.Rmd

# Рисование многоугольников, закрашивание областей графика

Polygon.html
Polygon.pdf
Polygon.Rmd

# Как нарисовать интеграл Римана, как управлять порядком наложения элементов графика

Riemann.html
Riemann.pdf
Riemann.Rmd

# Численное дифференцирование: как получить значение производной в заданной точке

Numeric_differentiation.html
Numeric_differentiation.pdf
Numeric_differentiation.Rmd
