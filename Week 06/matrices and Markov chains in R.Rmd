---
title: "Матрицы и цепи Маркова в R"
author: "Шалаев Н. Е."
date: '23 марта 2020 г.'
output:
  pdf_document:
    latex_engine: xelatex
  html_document: default
header-includes:
  - \usepackage{fontspec}
  - \setmainfont{PT Mono}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Обратная матрица 2х2 через детерминант

```{r}
X <- matrix(c(1,2,3,4), 2)
X
det(X)
solve(X)
```

```{r}
Xx <- matrix(c(4,-2,-3,1), 2)
Xx
Xx / det(X)
```

# Решение системы линейных уравнений

Решим следующую систему уравнений:

$$ 
\left\{
 \begin{array}{l}
  2x_1 - 3x_2 = 4\\
  5x_1 + 5x_2 = 3
 \end{array}
\right.
$$

В виде расширенной матрицы:

\[
\left[
\begin{array}{cc|c}
2 & -3 & 4 \\
5 & 5 & 3
\end{array}
\right]
\]

А в общей матричной форме:

$$ \textbf{A}\textbf{x} = \textbf{y}$$
$$\textbf{A} =  \left[
\begin{array}{cc} 2 & -3\\ 5 & 5 \end{array} \right],
\textbf{x} = \left[ \begin{array}{c} x_1\\ x_2 \end{array} \right],
\textbf{y} = \left[ \begin{array}{c} 4\\ 3 \end{array} \right]$$

Для решения нужно построить и инвертировать матрицу с коэффициентами при переменных $x_1$ и $x_2$, а затем умножить на вектор значений  $y$

Матрица коэффициентов:

```{r}
matrix(c(2,5,-3,5), nrow = 2)
```

Обратная матрица с помощью функции `solve()`:

```{r}
solve(matrix(c(2,5,-3,5), nrow = 2))
```

Скомпонуем это для выполнения матричной операции:

$$ \textbf{A}^{-1}\textbf{y} = \textbf{x} $$

```{r}
solve(matrix(c(2,5,-3,5), nrow = 2)) %*% c(4,3)
```

Следовательно, $$ x_1 = 1.16, x_2 = -0.56 $$

# Линейная регрессия через матричные операции

В матричном виде получение коэффициентов для регрессионной прямой записывается так:

$$ \textbf{b} = (\textbf{X}'\textbf{X})^{-1}\textbf{X}'\textbf{y} $$

Создадим примерный набор данных:

```{r}
Survey <- data.frame(Nation = c( "Netherlands", "Luxembourg", "Sweden",
					   "Germany", "Italy", "Spain", 
					   "Finland", "France", "United Kingdom", 
					   "Belgium", "Austria", "Denmark", 
					   "Portugal", "Greece", "Ireland"), 
RelativeIncome = c( 93.00, 99.00, 83.00, 
			  97.00, 96.00, 91.00, 
			  78.00, 90.00, 78.00, 
			  76.00, 84.00, 68.00, 
			  76.00, 74.00, 69.00),
PovertyRate = c( 7.00, 8.00, 8.00,
		     11.00, 14.00, 16.00,
		     17.00, 19.00, 21.00,
		     22.00, 24.00, 31.00,
		     33.00, 33.00, 34.00))
```

В качестве независимой переменной будет выступать RelativeIncome, а в качестве зависимой — PovertyRate.

Прежде всего, нужно получить матрицу X — для этого нужно добавить в качестве самой левой колонки вектор из единиц:

```{r}
x_matrix <- cbind(rep(1,nrow(Survey)), Survey$RelativeIncome)
```

Рассмотрим, как производится транспонирование матрицы `t()` (в данном случае заодно с умножением) и получение обратной матрицы `solve()`:

```{r}
t(x_matrix) %*% x_matrix
solve(t(x_matrix) %*% x_matrix)
```

Теперь мы можем объединить все функции R для исполнения нужной операции:

```{r}
solve(t(x_matrix) %*% x_matrix) %*% t(x_matrix) %*% Survey$PovertyRate
```

Проверим наш результат. Для этого применим стандартную функцию R `lm()`:

```{r}
lm(Survey$PovertyRate ~ Survey$RelativeIncome)
```

Или даже подробнее с помощью функции `summary()`:

```{r}
summary(lm(Survey$PovertyRate ~ Survey$RelativeIncome))
```

Легко убедиться, что результаты идентичны.

Отобразим результат на простом графике. Для этого мы используем функцию `plot()`, уже знакомую нам, и функцию `abline()`, которая наносит на график прямую, описываемую уравнением

$$ y = a + bx $$

```{r}
plot(x = Survey$RelativeIncome, 
     y = Survey$PovertyRate, 
     pch = 19, 
     xlab = "Poverty rate", 
     ylab = "Relative income")
abline(a = 83.69, b = -0.76, col = "blue", lw = 2)
```

# Цепи Маркова

В данном случае нас интересует одна операция — матричное умножение. Правда теперь его часто придётся повторять много раз подряд. В R есть для этого много способов, но для нас самым очевидным будет цикл for, который скорее всего будет знаком по школьной программе информатики. Также мы воспользуемся полезной возможностью давать имена рядам и столбцам матриц в R.

## Изменение числа пожертвователей

```{r}
contrprob <- matrix(c(0.97,0.75,0.03,0.25), 
			  nrow=2, 
			  dimnames = list(c("No donation", "Donation"), 
			  		    c("No donation", "Donation")))
contrprob
donors <- matrix(c(50,50), nrow=1)
tmp_d <- donors
for (i in seq(1,5)) { 
	tmp_d <- tmp_d %*% contrprob
	print(round(tmp_d,0))
	}
```

## Использование контрацепции на Барбадосе

```{r}
barbados1967 <- matrix(c(0.89,0.52,0.11,0.48), 
			     nrow=2, 
			     dimnames = list(c("Use", "No use"), c("Use", "No use")))
barbados1967
barbados_tmp <- barbados1967
for (i in seq(1,5)) { 
	barbados_tmp <- barbados_tmp %*% barbados1967; 
	print (1967+i); 
	print(barbados_tmp, digits = 2) 
	}

barbados1955 <- matrix(c(0.48,0.08,0.52,0.92), 
			     nrow=2, 
			     dimnames = list(c("Use", "No use"), c("Use", "No use")))
barbados1955
barbados_tmp <- barbados1955
for (i in seq(1,12)) { 
	barbados_tmp <- barbados_tmp %*% barbados1955; 
	print (1955+i); 
	print(barbados_tmp, digits = 2)
	}
```


## Кооперация и конфликты деревенек в Перу

```{r}
peru_communities <- matrix(c(1,0.25,0,0,0,0.75,0,0,0,0,0.33,0,0,0,0.67,1), 
				   nrow=4, 
				   dimnames = list(c("HcHx", "HcLx", "LcHx", "LcLx"), 
				   		    c("HcHx", "HcLx", "LcHx", "LcLx")))
peru_communities
```

```{r}
peru_tmp <- peru_communities
for (i in seq(1,20)) { 
	peru_tmp <- peru_tmp %*% peru_communities;
	print (paste(i,":",1969+i*5)); 
	print(round(peru_tmp,2)) 
	}
```



## Внутренняя миграция в Малави

Здесь мы дополнительно заведём таблицу с данными (data.frame) df1 для хранения всех промежуточных результатов:

```{r}
malawi1 <- matrix(c(0.97, 0.005, 0.004, 0.019, 0.983, 0.014, 0.012, 0.012, 0.982), 
			nrow = 3, 
			dimnames=list(c("North", "Center", "South"), c("North", "Center", "South")))
malawi1
```

Зададим исходное соотношение численности (11.7%, 38.5%, 49.8%) и проведем подсчёт:

```{r}
pop1 <- matrix(c(11.7, 38.5, 49.8), nrow=1)
df1 <- data.frame(North = pop1[1,1], Central = pop1[1,2], South = pop1[1,3])
for (i in seq(1,100)) { 
	pop1 <- pop1 %*% malawi1; 
	df1 <- rbind(df1, as.vector(pop1)); 
	print(pop1, digits = 3) 
	}
```

Все результаты выше были занесены в таблицу `df1`. Теперь мы можем построить график ожидаемого изменения населённости трёх регионов Малави со временем:

```{r}
plot(NA, type="n", 
     xlim=c(1,nrow(df1)), ylim=c(0,100), 
     ylab = "% Population", xlab = "Year")
lines(df1$North, col="Red", lw=2)
lines(df1$Central, col="Blue", lw=2)
lines(df1$South, col="Green", lw=2)

legend(80, 100, 
	 legend=c("North", "Center", "South"),
       col=c("red", "blue", "green"), 
	 lw=2)
```


