---
title: "Фракталы"
author: "Шалаев Н. Е."
date: '15 мая 2020 г.'
output:
  pdf_document:
    latex_engine: xelatex
  html_document: default
header-includes:
  - \usepackage{fontspec}
  - \setmainfont{PT Mono}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Недифференцируемые функции

$$ f(x) = |sin(x)|$$

```{r}
abs.sin <- function(x) {
	return( abs(sin(x)) )
}

x <- seq(0,13,0.001)
plot(x, 
     sin(x), 
     type="l", 
     lwd=1, 
     col="grey", 
     asp="1",
     ylab = "sin(x); |sin(x)|")
lines(x,
	abs.sin(x), 
	lwd=2, 
	col="navy")
abline(h=0, lty=2, col="grey")
```

# Функция Вейерштрасса

$$ F_W = \sum_{n=0}^{\infty}a^n\cos{(b^n \pi x)}$$

Где
$$ 0 < a < 1 $$
$$ b > 1: b \mod 2 = 1 $$

```{r}
weierstrass <- function(x, a, b) {
	out <- 0
	for (n in 0:100) {
		out <- out + (a^n)*cos((b^n)*pi*x)
	}
	return(out)
}
```

```{r}
a <- 0.5
b <- 5
```

```{r}
x <- seq(0,5,0.01)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
```

```{r}
x <- seq(0,2,0.01)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
```

```{r}
x <- seq(0,2,0.001)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
```

```{r}
x <- seq(0,2,0.0001)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
```
```{r}
x <- seq(0,2,0.0001)
y <- weierstrass(x,a,b)

plot(x,y,type = "l")
polygon(x=c(0.6,0.6,1,1), 
	  y=c(-2,0.3,0.3,-2), 
	  col=rgb(124/256, 252/256, 0, 0.25),
	  border=NA)
```


```{r}
x <- seq(0.6,1,0.0001)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
polygon(x=c(0.76,0.76,0.84,0.84), 
	  y=c(-1,0.2,0.2,-1), 
	  col=rgb(124/256, 252/256, 0, 0.25),
	  border = NA)
```

```{r}
x <- seq(0.76,0.84,0.0001)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
```
```{r}
x <- seq(0.76,0.84,0.00001)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
polygon(x=c(0.792,0.792,0.808,0.808), 
	  y=c(0.45,1,1,0.45), 
	  col=rgb(124/256, 252/256, 0, 0.25),
	  border = NA)
```

```{r}
x <- seq(0.792,0.808,0.00001)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
```

```{r}
x <- seq(0.792,0.808,0.000001)
y <- weierstrass(x,a,b)

plot(x,y, type = "l")
```

# Снежинка Коха

```{r}
library("alphahull")
```

```{r}
par(mfrow=c(2,2))

snowflake <- koch(3,1)
plot(snowflake, type="l", col="navy", asp=1, xlab=NA, ylab=NA)

snowflake <- koch(3,2)
plot(snowflake, type="l", col="navy", asp=1, xlab=NA, ylab=NA)

snowflake <- koch(3,3)
plot(snowflake, type="l", col="navy", asp=1, xlab=NA, ylab=NA)

snowflake <- koch(3,4)
plot(snowflake, type="l", col="navy", asp=1, xlab=NA, ylab=NA)

par(mfrow=c(1,1))
```

```{r}
snowflake <- koch(3,5)
plot(snowflake, type="l", col="navy", asp=1, xlab=NA, ylab=NA)
```

```{r}
snowflake <- koch(3,7)
plot(snowflake, type="l", col="navy", asp=1, xlab=NA, ylab=NA)
polygon(x=c(1,1,1.5,1.5), 
	  y=c(1,2,2,1), 
	  col=rgb(124/256, 252/256, 0, 0.25),
	  border = NA)
```

```{r}
plot(snowflake[which(snowflake[,1] > 1 & snowflake[,2] > 1),], 
     type="l", 
     col="navy", 
     asp=1, 
     xlab=NA, 
     ylab=NA)
polygon(x=c(1,1,1.5,1.5), 
	  y=c(1.5,2,2,1.5), 
	  col=rgb(124/256, 252/256, 0, 0.25),
	  border = NA)
```

```{r}
plot(snowflake[which(snowflake[,1] > 1 & snowflake[,2] > 1.5),], 
     type="l", 
     col="navy", 
     asp=1, 
     xlab=NA, 
     ylab=NA)
polygon(x=c(1.3,1.3,1.5,1.5), 
	  y=c(1.65,1.85,1.85,1.65), 
	  col=rgb(124/256, 252/256, 0, 0.25),
	  border = NA)
```

```{r}
plot(snowflake[which(snowflake[,1] > 1.3 & snowflake[,2] > 1.65),], 
     type="l",
     col="navy",
     asp=1, 
     xlab=NA,
     ylab=NA)
```

# Фрактальные измерения

$$ N_{small} = M^D $$
$$ \log{(N_{small})} = \log{(M^D)}$$
$$ \log{(N_{small})} = D\log{(M)}$$
$$ D = \frac{\log{(N_{small})}}{\log{(M)}}$$
```{r}
log(27)/log(3)
```


```{r}
population <- function(x, r) { 
	return( r*x*(1-x) )
}
```

```{r}
r <- 3.2
x <- 0.1
Xs1 <- c(x)
for (i in 1:200) {
	x <- population(x,r)
	Xs1 <- c(Xs1, x)
}
plot(x = 1:length(Xs1),
     y = Xs1,
     type = "l",
     lwd = 2,
     lty = 1,
     xlab = "t",
     ylab = "f(t)(x)")
```

```{r}
r <- 3.56
x <- 0.05
Xs2 <- c(x)
for (i in 1:200) {
	x <- population(x,r)
	Xs2 <- c(Xs2, x)
}
plot(x = 1:length(Xs2),
     y = Xs2,
     type = "l",
     lwd = 2,
     lty = 1,
     xlab = "t",
     ylab = "f(t)(x)")
```

```{r}
library("fractaldim")

fd.estim.boxcount(Xs1)$fd
fd.estim.boxcount(Xs2)$fd

fd.estim.boxcount(abs.sin(seq(0,13,0.01)))$fd

plot(seq(0,13,0.01), abs.sin(seq(0,13,0.01)), type="l")

fd.estim.boxcount(snowflake)$fd

log(4)/log(3)
```

```{r}
a <- 0.5
b <- 5

x <- seq(0.76,0.84,0.00001)
y <- weierstrass(x,a,b)

fd.estim.boxcount(y)$fd

2 + log(a, base = b)
```